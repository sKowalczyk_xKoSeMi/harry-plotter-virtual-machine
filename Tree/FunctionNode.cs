﻿namespace HarryPlotterVirtualMachine.Tree
{
    /// <summary>
    /// Węzeł zwracający wartość. Dziedziczą z niego funkcje oraz wartości dosłowne.
    /// </summary>
    /// <typeparam name="T">zwracany typ</typeparam>
    public abstract class FunctionNode<T> :Node
    {
        private T _value;

        /// <summary>
        /// Zwracana wartość. Przy próbie dostępu jest wykonywana metoda Execute().
        /// </summary>
        public T Value
        {
            get
            {
                Execute();
                return _value;
            }
            protected set => _value = value;
        }
    }
}