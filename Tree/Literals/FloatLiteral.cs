namespace HarryPlotterVirtualMachine.Tree.Literals
{
    /// <summary>
    /// Funkcja pełniąca rolę wartości dosłownej float.
    /// </summary>
    public class FloatLiteral : FunctionNode<double>
    {
        /// <summary>
        /// Jedyny konstruktor
        /// </summary>
        /// <param name="value">napis do przechowania</param>
        public FloatLiteral(double value)
        {
            Value = value;
        }

        /// <summary>
        /// <inheritdoc cref="Node"/>
        /// </summary>
        public override void Execute()
        {
        }
    }
}