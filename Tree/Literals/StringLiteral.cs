﻿namespace HarryPlotterVirtualMachine.Tree.Literals
{
    /// <summary>
    /// Funkcja pełniąca rolę wartości dosłownej string.
    /// </summary>
    public class StringLiteral :FunctionNode<string>
    {
        /// <summary>
        /// Jedyny konstruktor
        /// </summary>
        /// <param name="value">napis do przechowania</param>
        public StringLiteral(string value)
        {
            Value = value.Substring(1, value.Length-2);
        }

        /// <summary>
        /// <inheritdoc cref="Node"/>
        /// </summary>
        public override void Execute()
        {
        }
    }
}