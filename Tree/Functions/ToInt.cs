﻿using System;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    /// <summary>
    /// Klasa implementująca funkcję ToInt.
    /// </summary>
    /// <typeparam name="T">typ parametru</typeparam>
    public class ToInt<T> : FunctionNode<int>
    {
        private readonly FunctionNode<T> _f;

        public ToInt(FunctionNode<T> f)
        {
            _f = f;
        }

        public override void Execute()
        {
            switch (_f)
            {
                case FunctionNode<bool> b:
                    if (b.Value) Value = 1; 
                        else Value = 0;
                    break;
                case FunctionNode<double> _:
                case FunctionNode<string> _:
                    Value = Convert.ToInt32(_f.Value);
                    break;
            }
        }
    }
}
