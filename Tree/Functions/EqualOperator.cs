using System;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    /// <summary>
    /// Operator równania int.
    /// </summary>
    public class EqualInt : FunctionNode<bool>, IBinaryOperator<int>
    {
        private FunctionNode<int> _a;
        private FunctionNode<int> _b;

        public EqualInt()
        {
        }

        public EqualInt(FunctionNode<int> a, FunctionNode<int> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<int> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<int> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value == _b.Value;
        }
    }

    /// <summary>
    /// Operator równania float.
    /// </summary>
    public class EqualFloat : FunctionNode<bool>, IBinaryOperator<double>
    {
        private FunctionNode<double> _a;
        private FunctionNode<double> _b;

        public EqualFloat()
        {
        }

        public EqualFloat(FunctionNode<double> a, FunctionNode<double> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<double> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<double> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = Math.Abs(_a.Value - _b.Value) < 0.001;
        }
    }
    
    /// <summary>
    /// Operator porównywania string.
    /// </summary>
    public class EqualString : FunctionNode<bool>, IBinaryOperator<string>
    {
        private FunctionNode<string> _a;
        private FunctionNode<string> _b;

        public EqualString()
        {
        }

        public EqualString(FunctionNode<string> a, FunctionNode<string> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<string> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<string> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value.Equals(_b.Value);
        }
    }

    /// <summary>
    /// Operator logiczny AND.
    /// </summary>
    public class And : FunctionNode<bool>, IBinaryOperator<bool>
    {
        private FunctionNode<bool> _a;
        private FunctionNode<bool> _b;

        public And()
        {
        }

        public And(FunctionNode<bool> a, FunctionNode<bool> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<bool> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<bool> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value && _b.Value;
        }
    }
}