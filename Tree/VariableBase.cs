using System.Collections.Generic;
using HarryPlotterVirtualMachine.Tree.Procedures;

namespace HarryPlotterVirtualMachine.Tree
{
    /// <summary>
    /// Klasa przechowująca bazę zmiennych.
    /// </summary>
    /// <typeparam name="T">typ danych dla zmiennych</typeparam>
    public class VariableBase<T>
    {
        private readonly Dictionary<string, T> _variables;

        public VariableBase()
        {
            _variables = new Dictionary<string, T>();
        }

        /// <summary>
        /// Metoda sprawdzająca czy istnieje zmienna o takiej nazwie.
        /// </summary>
        /// <param name="variableName">nazwa zmiennej</param>
        /// <returns>wartość logiczna</returns>
        public bool Contains(string variableName)
        {
            return _variables.ContainsKey(variableName);
        }

        /// <summary>
        /// Metoda zwraca węzęł deklarujący zmienną.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <param name="value">węzeł zwracający wartość</param>
        /// <returns>węzeł</returns>
        public VariableDeclaration<T> Declare(string name, FunctionNode<T> value)
        {
            return new VariableDeclaration<T>(this, name, value);
        }

        /// <summary>
        /// Tworzenie nowej zmiennej.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <param name="value">wartość</param>
        public void Add(string name, T value)
        {
            _variables.Add(name, value);
        }

        /// <summary>
        /// Modyfikowanie zmiennej.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <param name="f">węzeł zwracający wartość</param>
        /// <returns>węzeł</returns>
        public SetVariable<T> Set(string name, FunctionNode<T> f)
        {
            return new SetVariable<T>(this, name, f);
        }

        /// <summary>
        /// Modyfikowanie zmiennej.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <param name="value">wartość</param>
        public void SetValue(string name, T value)
        {
            _variables[name] = value;
        }

        /// <summary>
        /// Pobranie wartości zmiennej.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <returns>węzeł zwracający wartość</returns>
        public FunctionNode<T> Get(string name)
        {
            return new GetVariable<T>(this, name);
        }

        /// <summary>
        /// Pobranie wartości zmiennej.
        /// </summary>
        /// <param name="name">nazwa</param>
        /// <returns>wartość</returns>
        public T GetValue(string name)
        {
            return _variables[name];
        }
    }
}