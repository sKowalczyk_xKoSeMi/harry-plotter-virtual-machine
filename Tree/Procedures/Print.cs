﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    public class Print : Node
    {
        private readonly FunctionNode<string> _value;

        /// <summary>
        /// Delegat umożliwający dodanie tekstu do okna konsoli.
        /// </summary>
        private AddTextDelegete _addTextToLogs;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">string lub funkcja typu string</param>
        /// <param name="addTextToLogs">delegat do wypisywania tekstu na konsolę</param>
        public Print(FunctionNode<string> value, AddTextDelegete addTextToLogs)
        {
            _value = value;
            _addTextToLogs = addTextToLogs;
        }

        /// <summary>
        /// Wypisanie napisu na ekran.
        /// </summary>
        public override void Execute()
        {
            _addTextToLogs(_value.Value);
        }
    }
}
