﻿using System;
using System.Windows.Forms;

namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    /// <summary>
    /// Klasa implementująca funkcję PrintLine.
    /// </summary>
    public class PrintLine : Node
    {
        private readonly FunctionNode<string> _value;

        /// <summary>
        /// Delegat umożliwający dodanie tekstu do okna konsoli.
        /// </summary>
        private AddTextDelegete _addTextToLogs;

        /// <summary>
        /// Konstruktor podstawowy i jedyny.
        /// </summary>
        public PrintLine()
        {
        }
        
        /// <summary>
        /// Konstruktor podstawowy i jedyny.
        /// </summary>
        /// <param name="value">string lub funkcja typu string</param>
        public PrintLine(FunctionNode<string> value)
        {
            _value = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">string lub funkcja typu string</param>
        /// <param name="addTextToLogs">delegat do wypisywania tekstu na konsolę</param>
        public PrintLine(FunctionNode<string> value, AddTextDelegete addTextToLogs)
        {
            _value = value;
            _addTextToLogs = addTextToLogs;
        }

        /// <summary>
        /// Wypisanie napisu na ekran.
        /// </summary>
        public override void Execute()
        {
            if (_value == null)
                Console.WriteLine();
            else
            {
                if (_addTextToLogs is null)
                {
                    Console.WriteLine(_value.Value);
                }
                else
                {
                    _addTextToLogs(_value.Value + "\n");
                }
            }
        }
    }
}