﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HarryPlotterVirtualMachine
{
    public delegate void AddTextDelegete(string pharse);
    public delegate string GetTypedTextDelegate();
    public delegate void ChangeEnabledTerminalStatusDelegate();
    public partial class MainView : Form
    {
        string typedText = "";
        private int currentLine = 0;
        private int currentColumn = 0;
        private AddTextDelegete addTextToLogs;
        private GetTypedTextDelegate getTypedText;
        private ChangeEnabledTerminalStatusDelegate changeTerminalEnabledStatus;

        public MainView()
        {
            InitializeComponent();
            CodeArea.Text = "";
            addTextToLogs = new AddTextDelegete(AddTextToLogs);
            getTypedText = new GetTypedTextDelegate(GetTypedText);
            changeTerminalEnabledStatus = new ChangeEnabledTerminalStatusDelegate(ChangeTerminalEnabledStatus);
        }

        private void SetCodeInfo()
        {
            CalculateCurrentLineNumber();
            CalculateCurrentColumnNumber();
            CalculateWordsCount();
        }

        private void CalculateCurrentLineNumber()
        {
            currentLine = CodeArea.GetLineFromCharIndex(CodeArea.SelectionStart);
            lineCount.Text = (currentLine + 1).ToString();
        }

        private void CalculateCurrentColumnNumber()
        {
            int line = CodeArea.GetLineFromCharIndex(CodeArea.SelectionStart);
            currentColumn = CodeArea.SelectionStart - CodeArea.GetFirstCharIndexFromLine(line);
            columnCount.Text = (currentColumn + 1).ToString();
        }

        private void CalculateWordsCount()
        {
            string typedCode = CodeArea.Text.Trim();
            typedCode = Regex.Replace(typedCode, @"\s+", " ");
            if (typedCode.Length == 0)
            {
                wordsCount.Text = "0";
            }
            else
            {
                int index = 0;
                int words = 1;
                while (index <= typedCode.Length - 1)
                {
                    if (typedCode[index] == ' ' || typedCode[index] == '\n' || typedCode[index] == '\t')
                    {
                        words++;
                    }
                    index++;
                }
                wordsCount.Text = words.ToString();
            }
        }

        private void SetSnippet(string pharse)
        {
            string[] lines = CodeArea.Lines;
            int startPosition = CodeArea.SelectionStart;
            if (lines.Length == 0)
            {
                CodeArea.Text = pharse;
            }
            else
            {
                lines[currentLine] += pharse;
                CodeArea.Lines = lines;
            }

            CodeArea.Select();
            CodeArea.SelectionStart = startPosition + pharse.Length;
            SetCodeInfo();
        }

        private void AddTextToLogs(string pharse)
        {
            //DateTime localDate = DateTime.Now;
            //LogsInfoText.Text = LogsInfoText.Text + localDate.ToString() + ": " +pharse;
            Invoke(new MethodInvoker(delegate
            {
                LogsInfoText.Text += pharse;
            }));
        }

        private void CodeArea_Click(object sender, EventArgs e)
        {
            SetCodeInfo();
        }

        private void CodeArea_KeyUp(object sender, KeyEventArgs e)
        {
            SetCodeInfo();
        }

        private void AuthorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AuthorsDialog authorsDialog = new AuthorsDialog();
            authorsDialog.Show();
        }


        private void VerionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VersionDialog versionDialog = new VersionDialog();
            versionDialog.Show();
        }

        private async void StartButton_Click(object sender, EventArgs e)
        {
            LogsInfoText.Text = "";
            Terminal.Text = "";
            AddTextToLogs("Program started!\n");

            RunCode();

        }

        private void RunCode()
        {
            string[] lines = CodeArea.Lines;
            Invoke(new MethodInvoker(delegate
            {
                Task task = Task.Run(() =>
                {
                    AstTest.Start(lines, addTextToLogs, getTypedText, changeTerminalEnabledStatus);
                });
            }));
        }

        private void ChangeTerminalEnabledStatus()
        {
            Invoke(new MethodInvoker(delegate
            {
                Terminal.Enabled = !Terminal.Enabled;
                if (Terminal.Enabled)
                {
                    InfoTab.SelectedIndex = 1;
                }
            }));
        }

        private void LogsInfoText_TextChanged(object sender, EventArgs e)
        {
            LogsInfoText.SelectionStart = LogsInfoText.Text.Length;
            LogsInfoText.ScrollToCaret();
        }

        private void CommentButton_Click(object sender, EventArgs e)
        {
            string commentText = "";
            string[] lines = CodeArea.SelectedText.Split(new string[] { "\n" }, StringSplitOptions.None);
            if (lines[lines.Length - 1] == "")
            {
                lines = lines.Take(lines.Count() - 1).ToArray();
            }
            if (lines.Length == 1)
            {
                commentText += "//" + lines[0];
            }
            else
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    commentText += "//" + lines[i] + "\n";
                }
            }
            CodeArea.SelectedText = commentText;

        }

        private void UncommentButton_Click(object sender, EventArgs e)
        {
            bool containComments = false;
            string commentdText = "";
            string[] lines = CodeArea.SelectedText.Split(new string[] { "\n" }, StringSplitOptions.None);
            if (lines.Length == 1)
            {
                if (lines[0].Substring(0, 2) == "//")
                {
                    commentdText += lines[0].Remove(0, 2);
                    containComments = true;
                }
            }
            else
            {
                for (int i = 0; i < lines.Length - 1; i++)
                {
                    if (lines[i].Substring(0, 2) == "//")
                    {
                        commentdText += lines[i].Remove(0, 2) + "\n";
                        containComments = true;
                    }
                }
            }
            if (containComments)
            {
                CodeArea.SelectedText = commentdText;
            }

        }

        private void CodeArea_TextChanged(object sender, EventArgs e)
        {
            //SetCommentsColor();
        }

        private void EmptyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CodeArea.Text = "";
        }

        private void UseTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CodeArea.Text = "";
            SetSnippet("please" + "\n" + "\n" + "thank you");
        }

        private void SaveToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SaveFileDialog dialog = new SaveFileDialog
            //{
            //    DefaultExt = "png",
            //    FileName = dialog.FileName,
            //    InitialDirectory = pathToModifiedPictures,
            //    Filter = "PNG images (*.png)|*.png"
            //};

            //if (dialog.ShowDialog() == DialogResult.OK)
            //{
            //    int width = Convert.ToInt32(PictureToSave.Width);
            //    int height = Convert.ToInt32(PictureToSave.Height);
            //    Bitmap bmp = new Bitmap(width, height);
            //    PictureToSave.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));

            //    bmp.Save(dialog.FileName, ImageFormat.Png);
            //}

            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "HPVM Files (*.hpvm)|*.hpvm";
                sfd.FilterIndex = 2;
                sfd.FileName = "Code.hpvm";
                sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, CodeArea.Text);
                }
            }
        }

        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileContent = string.Empty;
            string filePath = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                openFileDialog.Filter = "HPVM Files (*.hpvm)|*.hpvm";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        CodeArea.Text = reader.ReadToEnd();
                    }
                }
            }
        }



        private string GetTypedText()
        {
            string text = "";
                while (text == "")
                {
                    text = typedText;
                }
            typedText = "";
            return text;
        }

        private void Terminal_TextChanged(object sender, EventArgs e)
        {
            if (Terminal.Lines.Length > 1)
            {
                var lastLine = Terminal.Lines[Terminal.Lines.Length - 2];
                if (lastLine.Length > 0 && Terminal.Text[Terminal.Text.Length - 1] == '\n')
                {
                    typedText = lastLine;
                }
            }
        }

        private void SnippetList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string newLineAndSpacing = "\n";
            if (CodeArea.Lines.Length > 0)
            {
                string currentLineText = CodeArea.Lines[currentLine];
                char[] spaces = currentLineText.ToCharArray()
                            .Where(c => Char.IsWhiteSpace(c))
                            .ToArray();
                newLineAndSpacing = "\n" + new string(spaces);
            }
            switch (SnippetList.SelectedItem)
            {
                case "start-end -> [please...thank you]":
                    SetSnippet("please" + newLineAndSpacing + "\t" + newLineAndSpacing + "thank you");
                    break;

                case "string -> [czar]":
                    SetSnippet("czar ");
                    break;

                case "int -> [harry]":
                    SetSnippet("harry ");
                    break;

                case "float -> [ron]":
                    SetSnippet("ron ");
                    break;

                case "boolean -> [hermiona]":
                    SetSnippet("hermiona ");
                    break;

                case "true -> [fred]":
                    SetSnippet("fred");
                    break;

                case "false - [george]":
                    SetSnippet("george");
                    break;

                case "if() -> [elixir(){...}]":
                    SetSnippet("elixir()" + newLineAndSpacing + "{" + newLineAndSpacing + newLineAndSpacing + "}");
                    break;

                case "for() -> [parszywek(){...}]":
                    SetSnippet("parszywek()" + newLineAndSpacing + "{" + newLineAndSpacing + newLineAndSpacing + "}");
                    break;

                case "while() -> [hedwiga(){...}]":
                    SetSnippet("hedwiga()" + newLineAndSpacing + "{" + newLineAndSpacing + newLineAndSpacing + "}");
                    break;

                case "readLine() -> [alohomora()]":
                    SetSnippet("alohomora()" + newLineAndSpacing);
                    break;

                case "readIneeteger() -> [reducio()]":
                    SetSnippet("reducio()");
                    break;

                case "readFloat() -> [reducto()]":
                    SetSnippet("reducto()");
                    break;

                case "print(string) -> [lumos()]":
                    SetSnippet("lumos()" + newLineAndSpacing);
                    break;

                case "printLine(string) -> [accio()]":
                    SetSnippet("accio()" + newLineAndSpacing);
                    break;

                case "toString(float/int) -> [reparo()]":
                    SetSnippet("reparo()" + newLineAndSpacing);
                    break;

                case "toInt(string) -> [veraVerto()]":
                    SetSnippet("veraVerto()" + newLineAndSpacing);
                    break;

                case "toFloat(string) -> [vertoVera()]":
                    SetSnippet("vertoVera()" + newLineAndSpacing);
                    break;

                case "exit() -> [avadaKedavra()]":
                    SetSnippet("avadaKedavra()" + newLineAndSpacing);
                    break;

                default:
                    break;
            }
        }

        //private void SetCommentsColor()
        //{
        //    int index = CodeArea.SelectionStart;
        //    CodeArea.SelectionBackColor = Color.Transparent;
        //    string[] lines = CodeArea.Text.Split(new string[] { "\n" }, StringSplitOptions.None);
        //    for (int i = 0; i < lines.Length; i++)
        //    {
        //        if (lines[i].Length > 0)
        //        {
        //            if (lines[i].Length > 1 && lines[i].Substring(0, 2) == "//")
        //            {
        //                CodeArea.Find(lines[i]);
        //                CodeArea.SelectionColor = Color.Green;
        //            }
        //            else
        //            {
        //                if (CodeArea.SelectionColor == Color.Green)
        //                {
        //                    CodeArea.Find(lines[i]);
        //                    CodeArea.SelectionColor = Color.Black;
        //                }
        //            }
        //        }
        //    }
        //    CodeArea.SelectionStart = index;
        //}
    }
}
