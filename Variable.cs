﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine
{
    ///<summary>
    ///Klasa reprezentująca zmienne w programie (nazwa, wartość)
    ///</summary>
    public class Variable
    {
        public string Name { get; }
        public Type Value { get; }

        public Variable(string name, Type value)
        {
            Name = name;
            Value = value;
        }
    }
}