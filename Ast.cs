using System;
using System.Linq;
using System.Collections.Generic;
using HarryPlotterVirtualMachine.Tree;
using HarryPlotterVirtualMachine.Tree.Blocks;
using HarryPlotterVirtualMachine.Tree.Functions;
using HarryPlotterVirtualMachine.Tree.Literals;
using HarryPlotterVirtualMachine.Tree.Procedures;

namespace HarryPlotterVirtualMachine
{
    /// <summary>
    /// Klasa reprezentująca sparsowaną strukturę Abstract Syntax Tree.
    /// </summary>
    public partial class Ast
    {
        /// <summary>
        /// Korzeń drzewa.
        /// </summary>
        private Node _ast;

        /// <summary>
        /// Baza zmiennych int.
        /// </summary>
        private VariableBase<int> _intBase;

        /// <summary>
        /// Baza zmiennych float.
        /// </summary>
        private VariableBase<double> _floatBase;

        /// <summary>
        /// Baza zmiennych string.
        /// </summary>
        private VariableBase<string> _stringBase;

        /// <summary>
        /// Baza zmiennych boolean.
        /// </summary>
        private VariableBase<bool> _boolBase;

        /// <summary>
        /// Lista deklarowanych zmiennych.
        /// </summary>
        private readonly List<Variable> _variables;

        /// <summary>
        /// Delegat umożliwający dodanie tekstu do okna konsoli logów.
        /// </summary>
        private readonly AddTextDelegete _addTextToLogs;

        /// <summary>
        /// Delegat zmieniający status blokady w terminalu.
        /// </summary>
        private readonly ChangeEnabledTerminalStatusDelegate _changeTerminalStatus;

        /// <summary>
        /// Delegat pobierający ostatnią linię, po tym jak użytkownik wciśnie eneter w terminalu.
        /// </summary>
        private readonly GetTypedTextDelegate _getTypedText;

        /// <summary>
        /// Delegat oznaczający warunek zakończenia parsowania.
        /// </summary>
        /// <param name="l">aktualny lexem</param>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">index</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        private delegate bool EndParseCondition(Lexem l,
            IReadOnlyList<Lexem> list, int i, int lineNumber, char endChar);

        /// <summary>
        /// Domyślny konstruktor pustego drzewa.
        /// </summary>
        public Ast()
        {
        }

        /// <summary>
        /// Konstruktor budujący drzewo z leksemów z delegatem do wypisywania wyniku na okno logów 
        /// </summary>
        /// <param name="list">lista lexemów</param>
        /// <param name="variables">lista zmiennych</param>
        /// <param name="addTextToLogs">delegat do wypisania tekstu</param>
        /// <param name="getTypedText">delegat do pobrania tekstu</param>
        /// <param name="changeEnabledTerminalStatus"></param>
        public Ast(List<Lexem> list, List<Variable> variables,
            AddTextDelegete addTextToLogs, GetTypedTextDelegate getTypedText, 
            ChangeEnabledTerminalStatusDelegate changeEnabledTerminalStatus)
        {
            _variables = variables;
            _addTextToLogs = addTextToLogs;
            _changeTerminalStatus = changeEnabledTerminalStatus;
            _getTypedText = getTypedText;
            ParseLexems(list);
        }

        /// <summary>
        /// Metoda zamieniająca kod programu na abstrakcyjne drzewo składniowe.
        /// </summary>
        /// <param name="list">lista leksemów</param>
        public void ParseLexems(List<Lexem> list)
        {
            var i = 0;
            _intBase = new VariableBase<int>();
            _floatBase = new VariableBase<double>();
            _stringBase = new VariableBase<string>();
            _boolBase = new VariableBase<bool>();
            _ast = _parse(list, ref i, char.MinValue, 0, null);
        }

        /// <summary>
        /// Uruchomienie aktualnie zapisanego drzewa.
        /// </summary>
        public void Run()
        {
            var n = _ast;
            while (n != null)
            {
                n.Execute();
                n = n.Next;
            }
        }
        
        /// <summary>
        /// Rekurencyjna metoda parsująca leksemy od danego indeksu do danego znaku lub końca linii.
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index aktualnego leksemu</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endParseCondition">warunek stopu</param>
        /// <returns>sparsowany węzeł/poddrzewo</returns>
        private Node _parse(IReadOnlyList<Lexem> list, ref int i, char endChar,
            int lineNumber, EndParseCondition endParseCondition)
        {
            Node currentNode = null, startNode = null;

            for (; i < list.Count; i++)
            {
                var l = list[i];

                if (endParseCondition != null && endParseCondition(l, list, i, lineNumber, endChar))
                {
                    if (l.Text[0] != endChar) i--;
                    break;
                }

                if (l.Text.Equals(KeyWords.START)) continue;

                if (l.Text.Equals(KeyWords.END1)
                    || l.Text.Equals(KeyWords.END2)) break;

                switch (l.Text)
                {
                    case "{":
                        i++;
                        _append(ref startNode, ref currentNode,
                            _parse(list, ref i, '}', lineNumber, AstDelegate.CheckByChar));
                        continue;
                    case "(":
                        i++;
                        _append(ref startNode, ref currentNode,
                            _parse(list, ref i, ')', lineNumber, AstDelegate.CheckByChar));
                        continue;
                }

                if (_literalSwitch(l, list, ref i, endChar, ref startNode, ref currentNode)) continue;

                if (_comparingOperatorSwitch(l, list, ref i, endChar,
                    ref startNode, ref currentNode, endParseCondition))
                {
                    continue;
                }

                if (_mathOperatorSwitch(l, list, ref i, endChar,
                    ref startNode, ref currentNode, endParseCondition))
                {
                    continue;
                }

                if (_procedureSwitch(l, list, ref i, endChar,
                    ref startNode, ref currentNode, endParseCondition))
                {
                    continue;
                }

                _setVariable(list, ref i, ref startNode, ref currentNode, endChar, endParseCondition);
            }

            return startNode;
        }

        /// <summary>
        /// Metoda zajmuje się węzłami operatorów matematycznych.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endParseCondition">warunek stopu</param>
        private bool _mathOperatorSwitch(Lexem l, IReadOnlyList<Lexem> list, ref int i, char endChar,
            ref Node startNode, ref Node currentNode, EndParseCondition endParseCondition)
        {
            if (!IsMathOperator(l)) return false;

            i++;
            switch (l.Text)
            {
                case "+":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, AstDelegate.CheckMath, new SumInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, AstDelegate.CheckMath, new SumFloat());
                            break;
                        case FunctionNode<string> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, AstDelegate.CheckMath, new Concat());
                            break;
                    }

                    break;
                case "-":
                    if (currentNode == null)
                    {
                        _createNegationOperator(l, list, ref i, endChar,
                            ref startNode, ref currentNode, endParseCondition);
                        break;
                    }

                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new SubtractionInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new SubtractionFloat());
                            break;
                    }

                    break;
                case "*":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new MultiplicationInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new MultiplicationFloat());
                            break;
                    }

                    break;
                case "/":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new DivisionInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, endParseCondition, new DivisionFloat());
                            break;
                    }

                    break;
                case "%":
                    _createBinaryOperator(l, list, ref i, endChar,
                        ref startNode, ref currentNode, endParseCondition, new RestOfDivision());
                    break;
            }

            return true;
        }

        /// <summary>
        /// Metoda zajmuje się węzłami operatorów logicznych.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endParseCondition">warunek stopu</param>
        private bool _comparingOperatorSwitch(Lexem l, IReadOnlyList<Lexem> list, ref int i, char endChar,
            ref Node startNode, ref Node currentNode, EndParseCondition endParseCondition)
        {
            if (!IsComparingOperator(l) && !IsBoolOperator(l)) return false;

            i++;
            var end = endParseCondition ?? AstDelegate.CheckByChar;
            switch (l.Text)
            {
                case "==":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new EqualInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new EqualFloat());
                            break;
                        case FunctionNode<string> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new EqualString());
                            break;
                    }

                    break;
                case "&&":
                    _createBinaryOperator(l, list, ref i, endChar,
                        ref startNode, ref currentNode, end, new And());
                    break;
                case "||":
                    _createBinaryOperator(l, list, ref i, endChar,
                        ref startNode, ref currentNode, end, new Or());
                    break;
                case "<":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new LessInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new LessFloat());
                            break;
                    }

                    break;
                case "<=":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new LessEqualInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new LessEqualFloat());
                            break;
                    }

                    break;
                case ">":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new MajorInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new MajorFloat());
                            break;
                    }

                    break;
                case ">=":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new MajorEqualInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new MajorEqualFloat());
                            break;
                    }

                    break;
                case "!=":
                    switch (currentNode)
                    {
                        case FunctionNode<int> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new NotEqualInt());
                            break;
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new NotEqualFloat());
                            break;
                    }

                    break;
                case "!":
                    i++;
                    _createNegationOperator(l, list, ref i, endChar,
                            ref startNode, ref currentNode, endParseCondition);
                    
                    /*throw new NotImplementedException();*/
                    /*switch (currentNode)
                    {
                        case FunctionNode<double> _:
                            _createBinaryOperator(l, list, ref i, endChar,
                                ref startNode, ref currentNode, end, new NegationBoolOperator());
                            break;
                    }*/
                    break;
            }

            return true;
        }

        /// <summary>
        /// Metoda zajmująca węzłami głównych funkcji.
        /// </summary>
        /// <param name="l">aktualny lexem</param>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endParseCondition">warunek stopu</param>
        private bool _procedureSwitch(Lexem l, IReadOnlyList<Lexem> list, ref int i, char endChar,
            ref Node startNode, ref Node currentNode, EndParseCondition endParseCondition)
        {
            string name;
            FunctionNode<string> sNode;
            object oNode;
            var end = endParseCondition ?? AstDelegate.CheckByLine;

            switch (l.Text)
            {
                case KeyWords.TRUE:
                    _append(ref startNode, ref currentNode, new BooleanLiteral(true));
                    break;
                case KeyWords.FALSE:
                    _append(ref startNode, ref currentNode, new BooleanLiteral(false));
                    break;
                case KeyWords.STRING:
                    name = list[i + 1].Text;
                    i += 3;
                    var stringTmp =
                        (FunctionNode<string>) _parse(list, ref i, endChar, list[i].LineNumber, end);

                    if (end == AstDelegate.CheckByChar) i--;

                    _append(ref startNode, ref currentNode,
                        new VariableDeclaration<string>(_stringBase, name, stringTmp));
                    break;
                case KeyWords.INT:
                    name = list[i + 1].Text;
                    i += 3;
                    var intTmp = (FunctionNode<int>) _parse(list, ref i, endChar, list[i].LineNumber, end);
                    if (end == AstDelegate.CheckByChar) i--;

                    _append(ref startNode, ref currentNode,
                        new VariableDeclaration<int>(_intBase, name, intTmp));
                    break;
                case KeyWords.FLOAT:
                    name = list[i + 1].Text;
                    i += 3;
                    var floatTmp = (FunctionNode<double>) _parse(list, ref i, endChar, list[i].LineNumber, end);
                    if (end == AstDelegate.CheckByChar) i--;

                    _append(ref startNode, ref currentNode,
                        new VariableDeclaration<double>(_floatBase, name, floatTmp));
                    break;
                case KeyWords.BOOLEAN:
                    name = list[i + 1].Text;
                    i += 3;
                    var boolTmp = (FunctionNode<bool>) _parse(list, ref i, endChar, list[i].LineNumber, end);
                    if (end == AstDelegate.CheckByChar) i--;

                    _append(ref startNode, ref currentNode, new VariableDeclaration<bool>(_boolBase, name, boolTmp));
                    break;
                case KeyWords.TO_STRING:
                    i += 2;
                    oNode = _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    switch (oNode)
                    {
                        case FunctionNode<int> node:
                            _append(ref startNode, ref currentNode,
                                new ToString<int>(node));
                            break;
                        case FunctionNode<double> node:
                            _append(ref startNode, ref currentNode,
                                new ToString<double>(node));
                            break;
                        case FunctionNode<bool> node:
                            _append(ref startNode, ref currentNode,
                                new ToString<bool>(node));
                            break;
                        case FunctionNode<string> node:
                            _append(ref startNode, ref currentNode, node);
                            break;
                    }

                    break;
                case KeyWords.TO_INT:
                    i += 2;
                    oNode = _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    switch (oNode)
                    {
                        case FunctionNode<double> node:
                            _append(ref startNode, ref currentNode,
                                new ToInt<double>(node));
                            break;
                        case FunctionNode<bool> node:
                            _append(ref startNode, ref currentNode,
                                new ToInt<bool>(node));
                            break;
                        case FunctionNode<string> node:
                            _append(ref startNode, ref currentNode,
                                new ToInt<string>(node));
                            break;
                    }

                    break;
                case KeyWords.TO_FLOAT:
                    i += 2;
                    oNode = _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    switch (oNode)
                    {
                        case FunctionNode<double> node:
                            _append(ref startNode, ref currentNode,
                                new ToFloat<double>(node));
                            break;
                        case FunctionNode<bool> node:
                            _append(ref startNode, ref currentNode,
                                new ToFloat<bool>(node));
                            break;
                        case FunctionNode<string> node:
                            _append(ref startNode, ref currentNode,
                                new ToFloat<string>(node));
                            break;
                    }

                    break;
                case KeyWords.PRINT:
                    i += 2;
                    sNode = (FunctionNode<string>) _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    _append(ref startNode, ref currentNode, new Print(sNode, _addTextToLogs));
                    break;
                case KeyWords.PRINT_LINE:
                    i += 2;
                    sNode = (FunctionNode<string>) _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    _append(ref startNode, ref currentNode, new PrintLine(sNode, _addTextToLogs));
                    break;
                case KeyWords.READ_LINE:
                    _append(ref startNode, ref currentNode, new ReadLine(_changeTerminalStatus, _getTypedText));
                    break;
                case KeyWords.READ_INTEGER:
                    _append(ref startNode, ref currentNode, new ReadInteger(_changeTerminalStatus, _getTypedText));
                    break;
                case KeyWords.READ_FLOAT:
                    _append(ref startNode, ref currentNode, new ReadFloat(_changeTerminalStatus, _getTypedText));
                    break;
                case KeyWords.EXIT:
                    _append(ref startNode, ref currentNode, new ExitFunction(_addTextToLogs));
                    break;
                case KeyWords.IF:
                case KeyWords.FOR:
                case KeyWords.WHILE:
                    var n = _createBlockNode(list, ref i);
                    _append(ref startNode, ref currentNode, n);
                    break;
                case "!":
                    i++;
                    _createNegationOperator(l, list, ref i, endChar,
                        ref startNode, ref currentNode, endParseCondition);
                    break;
                default:
                    var variableName = l.Text;

                    if (list[i + 1].Text == "=") return false;

                    if (_intBase.Contains(variableName))
                        _append(ref startNode, ref currentNode, _intBase.Get(variableName));
                    else if (_floatBase.Contains(variableName))
                        _append(ref startNode, ref currentNode, _floatBase.Get(variableName));
                    else if (_stringBase.Contains(variableName))
                        _append(ref startNode, ref currentNode, _stringBase.Get(variableName));
                    else if (_boolBase.Contains(variableName))
                        _append(ref startNode, ref currentNode, _boolBase.Get(variableName));
                    else
                        return false;

                    return true;
            }

            return true;
        }

        /// <summary>
        /// Metoda tworząca węzeł blokowy if, while, for.
        /// </summary>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">index lexemu</param>
        /// <returns>węzeł</returns>
        private Node _createBlockNode(IReadOnlyList<Lexem> list, ref int i)
        {
            Node startNode = null;
            FunctionNode<bool> condition;
            Node block;
            var l = list[i];
            Node currentNode = null;
            switch (l.Text)
            {
                case KeyWords.IF:
                    i += 2;
                    condition = (FunctionNode<bool>) _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    i += 2;
                    block = _parse(list, ref i, '}', 0, AstDelegate.CheckByChar);
                    Node el = null;
                    if (i < list.Count - 1 && list[i + 1].Text.Equals(KeyWords.ELSE))
                    {
                        i++;
                        if (list[i + 1].Text.Equals(KeyWords.IF))
                        {
                            i += 1;
                            el = _createBlockNode(list, ref i);
                        }
                        else
                        {
                            i += 2;
                            el = _parse(list, ref i, '}', 0, AstDelegate.CheckByChar);
                        }
                    }

                    _append(ref startNode, ref currentNode, new If(condition, block, el));
                    break;
                case KeyWords.WHILE:
                    i += 2;
                    condition = (FunctionNode<bool>) _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    i += 2;
                    block = _parse(list, ref i, '}', 0, AstDelegate.CheckByChar);

                    _append(ref startNode, ref currentNode, new While(condition, block));
                    break;
                case KeyWords.FOR:
                    i += 2;
                    var declaration = _parse(list, ref i, ';', 0, AstDelegate.CheckByChar);
                    i++;
                    condition = (FunctionNode<bool>) _parse(list, ref i, ';', 0, AstDelegate.CheckByChar);
                    i++;
                    var change = _parse(list, ref i, ')', 0, AstDelegate.CheckByChar);
                    i += 2;
                    block = _parse(list, ref i, '}', 0, AstDelegate.CheckByChar);

                    _append(ref startNode, ref currentNode, new For(declaration, condition, change, block));
                    break;
            }

            return currentNode;
        }

        /// <summary>
        /// Tworzenie węzła przypisującego wartość do zmiennej.
        /// </summary>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">index lexemu</param>
        /// <param name="start">początkowy węzeł</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endChar">znak końcowy</param>
        /// <param name="endParseCondition">warunek zakończenia</param>
        private void _setVariable(IReadOnlyList<Lexem> list, ref int i, ref Node start, ref Node currentNode,
            char endChar, EndParseCondition endParseCondition)
        {
            var l = list[i];
            var name = l.Text;
            var fInt = _intBase.Contains(name);
            var fFloat = _floatBase.Contains(name);
            var fBoolean = _boolBase.Contains(name);
            var fString = _stringBase.Contains(name);

            i += 2;
            var end = endParseCondition ?? AstDelegate.CheckByLine;
            if (endChar == '}') end = AstDelegate.CheckByLine;

            var value = _parse(list, ref i, endChar, l.LineNumber, end);

            if (i < list.Count && list[i].Text[0] == endChar) i--;

            if (fInt)
            {
                _append(ref start, ref currentNode,
                    new SetVariable<int>(_intBase, name, (FunctionNode<int>) value));
            }
            else if (fFloat)
            {
                _append(ref start, ref currentNode,
                    new SetVariable<double>(_floatBase, name, (FunctionNode<double>) value));
            }
            else if (fString)
            {
                _append(ref start, ref currentNode,
                    new SetVariable<string>(_stringBase, name, (FunctionNode<string>) value));
            }
            else if (fBoolean)
            {
                _append(ref start, ref currentNode,
                    new SetVariable<bool>(_boolBase, name, (FunctionNode<bool>) value));
            }
        }

        /// <summary>
        /// Metoda tworząca operator z jednym argumentem.
        /// </summary>
        /// <param name="l">aktualny lexem</param>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endParseCondition">warunek stopu</param>
        private void _createNegationOperator(Lexem l, IReadOnlyList<Lexem> list,
            ref int i, char endChar, ref Node startNode, ref Node currentNode,
            EndParseCondition endParseCondition)
        {
            var t = TypeOfFirstLiteral(list, i);
            var a = _parse(list, ref i, endChar, l.LineNumber, endParseCondition);
            Node n;
            
            if (l.Text.Equals("-"))
            {
                if (t == typeof(int))
                    n = new NegationIntOperator((FunctionNode<int>) a);
                else
                    n = new NegationFloatOperator((FunctionNode<double>) a);
            }
            else
            {
                if (t == typeof(string))
                    n = new NegationStringOperator((FunctionNode<string>) a);
                else
                    n = new NegationBoolOperator((FunctionNode<bool>) a);
            }

            // EndParseCondition end;
            // if (IsComparingOperator(l))
            // {
            //     // porównywanie kończy na operatorach boolean
            //     end = !IsBoolOperator(l) ? AstDelegate.CheckByLineCharBoolean : endParseCondition;
            //     
            //     var end1 = end;
            //     end = delegate(Lexem lexem, IReadOnlyList<Lexem> lexems, int i1, int number, char endChar1)
            //     {
            //         var tmp1 = end1(lexem, lexems, i1, number, endChar1);
            //         var tmp2 = IsBoolOperator(lexem);
            //         return tmp1 || tmp2;
            //     };
            // }
            // else
            // {
            //     end = endParseCondition;
            // }

            // var a = currentNode;
            // var b = _parse(list, ref i, endChar, l.LineNumber, end);
            // n.SetA((FunctionNode<T>) a);
            // n.SetB((FunctionNode<T>) b);

            if (list[i].Text[0] == endChar) i--;

            if (startNode == currentNode)
                startNode = n;
            currentNode = n;
        }

        /// <summary>
        /// Metoda tworząca operator z dwoma argumentami.
        /// </summary>
        /// <param name="l">aktualny lexem</param>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="endParseCondition">warunek stopu</param>
        /// <param name="n">węzeł operatora</param>
        /// <typeparam name="T">typ danych argumentów</typeparam>
        private void _createBinaryOperator<T>(Lexem l, IReadOnlyList<Lexem> list,
            ref int i, char endChar, ref Node startNode,
            ref Node currentNode, EndParseCondition endParseCondition, IBinaryOperator<T> n)
        {
            EndParseCondition end;

            if (IsComparingOperator(l) || IsMathOperator(l))
            {
                // porównywanie kończy na operatorach boolean
                end = !IsBoolOperator(l) ? AstDelegate.CheckByLineCharBoolean : endParseCondition;

                var end1 = end;
                end = delegate(Lexem lexem, IReadOnlyList<Lexem> lexems, int i1, int number, char endChar1)
                {
                    var tmp1 = end1(lexem, lexems, i1, number, endChar1);
                    var tmp2 = IsBoolOperator(lexem);
                    return tmp1 || tmp2;
                };
            }
            else
            {
                end = endParseCondition;
            }

            var a = currentNode;
            var b = _parse(list, ref i, endChar, l.LineNumber, end);

            n.SetA((FunctionNode<T>) a);
            n.SetB((FunctionNode<T>) b);

            if (list[i].Text[0] == endChar) i--;

            var c = (Node) n;

            if (startNode == currentNode)
                startNode = c;
            currentNode = c;
        }

        /// <summary>
        /// Metoda dopisująca nowy węzeł do aktualnego drzewa. Jeśli drzewo jest puste to tworzy nowy korzeń.
        /// </summary>
        /// <param name="start">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <param name="next">nowy węzeł</param>
        private static void _append(ref Node start, ref Node currentNode, Node next)
        {
            if (currentNode == null)
            {
                currentNode = next;
                start = next;
            }
            else
            {
                if (currentNode.Next != null)
                    currentNode = currentNode.Next;
                currentNode.Next = next;
            }
        }
        
        /// <summary>
        /// Metoda zajmuje się węzłami wartości dosłownych.
        /// </summary>
        /// <param name="l">aktualny lexem</param>
        /// <param name="list">lista lexemów</param>
        /// <param name="i">aktualny index listy</param>
        /// <param name="endChar">znak kończący sekwencję</param>
        /// <param name="startNode">początek drzewa</param>
        /// <param name="currentNode">aktualny węzeł</param>
        /// <returns>obsłużono lexem</returns>
        private static bool _literalSwitch(Lexem l, IReadOnlyList<Lexem> list, ref int i, char endChar,
            ref Node startNode, ref Node currentNode)
        {
            if (IsString(l))
            {
                _append(ref startNode, ref currentNode, new StringLiteral(l.Text));
                return true;
            }

            if (IsInt(l, out var tmp))
            {
                _append(ref startNode, ref currentNode, new IntLiteral(tmp));
                return true;
            }

            if (!IsFloat(l, out var d)) return false;

            _append(ref startNode, ref currentNode, new FloatLiteral(d));
            return true;
        }

        /// <summary>
        /// Metoda sprawdzająca czy lexem jest liczbą zmiennoprzecinkową.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <param name="d">parsowana liczba</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsFloat(Lexem l, out double d)
        {
            return double.TryParse(l.Text.Replace('.', ','), out d);
        }

        /// <summary>
        /// Metoda sprawdzająca czy lexem jest napisem.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsString(Lexem l)
        {
            return l.Text[0] == '\"' || l.Text[0] == '\'';
        }

        /// <summary>
        /// Metoda sprawdzająca czy lexem jest liczbą całkowitą.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <param name="i">parsowana liczba</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsInt(Lexem l, out int i)
        {
            return int.TryParse(l.Text, out i);
        }

        /// <summary>
        /// Metoda sprawdzająca typ pierwszego napotkanego literału.
        /// </summary>
        /// <param name="list">lista lexemów</param>
        /// <param name="index">index w liście</param>
        /// <returns>typ</returns>
        public Type TypeOfFirstLiteral(IReadOnlyList<Lexem> list, int index)
        {
            do
            {
                var l = list[index++];
                var s = l.Text;
                if (s.Equals(KeyWords.TRUE) || s.Equals(KeyWords.FALSE)) 
                    return typeof(bool);

                var v = _variables.FirstOrDefault(v2 => v2.Name.Equals(s));

                if (v != null) return v.Value;
                if (IsInt(l, out _)) return typeof(int);
                if (IsFloat(l, out _)) return typeof(float);
                if (IsString(l)) return typeof(string);
            } while (index < list.Count);
            
            return null;
        }

        /// <summary>
        /// Metoda sprawdzająca typ literału lub zmiennej.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <param name="variableList">lista zmiennych</param>
        /// <returns>typ wyrażenia, null jeśli niepoprawne</returns>
        public static Type GetType(Lexem l, List<Variable> variableList)
        {
            if (IsInt(l, out _))
                return typeof(int);
            if (IsFloat(l, out _))
                return typeof(float);
            if (IsString(l)) return typeof(string);

            var tmp = variableList.FirstOrDefault(variable => variable.Name.Equals(l.Text));
            return tmp?.Value;
        }

        public static bool IsVariable(Lexem l, List<Variable> variableList)
        {
            return variableList.Count(variable => variable.Name.Equals(l.Text)) > 0;
        }

        /// <summary>
        /// Metoda sprawdzająca czy lexem jest operatorem porównania.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsComparingOperator(Lexem l)
        {
            var array = new[] {">", "<", ">=", "<=", "!=", "=="};
            return array.Contains(l.Text);
        }

        /// <summary>
        /// Sprawdzenie czy lexem jest operatorem logicznym.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsBoolOperator(Lexem l)
        {
            var array = new[] {"&&", "||"};
            return array.Contains(l.Text);
        }

        /// <summary>
        /// Metoda sprawdzająca czy lexem jest operatorem.
        /// </summary>
        /// <param name="l">lexem</param>
        /// <returns>wartość logiczna</returns>
        public static bool IsMathOperator(Lexem l)
        {
            var array = new[] {"+", "-", "*", "/", "%"};
            return array.Contains(l.Text);
        }
    }
}